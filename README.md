# Diceware BASH Script

BASH script to generate diceware passwords. Results are output to stdout and are ephemeral once the terminal is closed as they are not saved to disk. The EFF long word list is included with the script. Please place your custom dictionary file in the same directory as the script and specify your custom dictionary file name (case sensitive) within the script.