#!/bin/bash

# NOTES:
# The EFF long word list is included with the script. Please place
# your custom dictionary file in the same directory as the script.
# Specify the case sensitive dictionary file name below.
dictName="eff_large_wordlist.txt"

#delcare variables
BASEDIR="$(dirname "$(readlink -f "$0")")"
dictLoc="$BASEDIR/$dictName"	#enter dictionary location
read -p "Passphrase length: " passLength	#length of passphrase


for i in `seq 1 $passLength`;
do
	read -p "Dice sequence: " diceSeq
	grep $diceSeq $dictLoc
	diceSeq=00000;
done;
